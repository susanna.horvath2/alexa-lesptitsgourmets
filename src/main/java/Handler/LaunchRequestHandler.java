package Handler;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.LaunchRequest;
import com.amazon.ask.model.Response;
import com.amazon.ask.request.Predicates;

import java.util.Optional;

public class LaunchRequestHandler implements RequestHandler {

    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(Predicates.requestType(LaunchRequest.class));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {
        String speechText = "Bienvenu sur les ptits gourmets.";
        return input.getResponseBuilder()
                .withSpeech(speechText)
                .withSimpleCard("Les ptits gourmets", speechText)
                .withShouldEndSession(false)
                .withReprompt(speechText)
                .build();
    }

}
package Handler;

import Service.RecipeService;
import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.*;
import com.amazon.ask.request.Predicates;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static Constant.Constants.*;


public class RecipeHandler implements RequestHandler {
    @Override
    public boolean canHandle(HandlerInput handlerInput) {
        return handlerInput.matches(Predicates.intentName("Recipe"));
    }

    @Override
    public Optional<Response> handle(HandlerInput handlerInput) {
        Request request = handlerInput.getRequestEnvelope().getRequest();
        IntentRequest intentRequest = (IntentRequest) request;
        Intent intent = intentRequest.getIntent();
        Map<String, Slot> slots = intent.getSlots();

        Slot recipeNameSlot = slots.get(RECIPE_NAME_SLOT);
        String recipeName = recipeNameSlot.getValue();
        String speechText;

        RecipeService recipeService = new RecipeService();

        JSONObject recipeLight = recipeService.getRecipeFromName(recipeName);
        if(recipeLight != null) {
            Integer recipeId = (Integer) recipeLight.get("id");
            JSONArray steps = recipeService.getStepsFromRecipeId(recipeId);

            if(steps != null) {
                speechText = "Vous avez choisi la recette : " + recipeName;
                speechText += "<break time=\"1s\"/>";
                speechText += "Etape " + steps.getJSONObject(0).get("noorder");
                speechText += "<break time=\"200ms\"/>";
                speechText += steps.getJSONObject(0).get("description");

                Map<String, Object> sessionAttributes = new HashMap<>();
                sessionAttributes.put(RECIPE_NAME_KEY, Integer.valueOf(recipeId));
                sessionAttributes.put(RECIPE_STEP_ID, 0);
                handlerInput.getAttributesManager().setSessionAttributes(sessionAttributes);
            } else {
                speechText = "Erreur durant la récupération des étapes de la recette";
            }
        } else {
            speechText = "Erreur durant la récupération de la recette : " + recipeName;
        }

        return handlerInput.getResponseBuilder()
                .withSpeech(speechText)
                .withSimpleCard("Recette : ", speechText)
                .withReprompt(speechText)
                .withShouldEndSession(false)
                .build();
    }
}

package Handler;

import Service.RecipeService;
import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import com.amazon.ask.request.Predicates;
import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static Constant.Constants.RECIPE_NAME_KEY;
import static Constant.Constants.RECIPE_STEP_ID;

public class NextStepHandler implements RequestHandler {
    @Override
    public boolean canHandle(HandlerInput handlerInput) {
        return handlerInput.matches(Predicates.intentName("NextStep"));
    }

    @Override
    public Optional<Response> handle(HandlerInput handlerInput) {
        String speechText = "";
        Integer idStep = (Integer) handlerInput.getAttributesManager().getSessionAttributes().get(RECIPE_STEP_ID);
        Integer recipeId = (Integer) handlerInput.getAttributesManager().getSessionAttributes().get(RECIPE_NAME_KEY);

        RecipeService recipeService = new RecipeService();
        JSONArray steps = recipeService.getStepsFromRecipeId(recipeId);
        if (steps != null) {
            if (idStep == steps.length()) {
                idStep = steps.length();
                speechText += "Dernière étape";
            } else {
                idStep++;
                speechText += "Etape " + steps.getJSONObject(idStep).get("noorder");
            }

            speechText += "<break time=\"200ms\"/>";
            speechText += steps.getJSONObject(idStep).get("description");


            Map<String, Object> sessionAttributes = new HashMap<>();
            sessionAttributes.put(RECIPE_NAME_KEY, Integer.valueOf(recipeId));
            sessionAttributes.put(RECIPE_STEP_ID, idStep);
            handlerInput.getAttributesManager().setSessionAttributes(sessionAttributes);
        } else {
            //Erreur durant la récupération des steps
            speechText = "Erreur durant la récupération des étapes de la recette";
        }

        return handlerInput.getResponseBuilder()
                .withSpeech(speechText)
                .withSimpleCard("Recette : ", speechText)
                .withReprompt(speechText)
                .withShouldEndSession(false)
                .build();


    }
}

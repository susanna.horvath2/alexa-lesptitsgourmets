package Service;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import static Constant.Constants.URL_API;

public class RecipeService {

    public JSONObject getRecipeFromName(String recipeName) {
        JSONObject responseAll = null;
        try {
            responseAll = ApiService.readJsonFromUrl(URL_API + "recipes/?name=" + recipeName);
            JSONObject recipeLight = responseAll.getJSONArray("data").getJSONObject(0);
            return recipeLight;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONArray getStepsFromRecipeId(Integer recipeId) {
        try {
            JSONObject responseOne = ApiService.readJsonFromUrl(URL_API + "recipe/" + recipeId);
            JSONObject recipeFull = responseOne.getJSONArray("data").getJSONObject(0);
            JSONArray steps = recipeFull.getJSONArray("steps");
            return steps;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
